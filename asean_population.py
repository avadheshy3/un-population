import csv
import matplotlib.pylab as plt
countries = ["Brunei Darussalam", "Cambodia", "Indonesia",
             "Lao People's Democratic Republic", "Malaysia", "Myanmar",
             "Philippines", "Singapore", "Thailand", "Viet Nam"]


def read_data():
    asean_population = {}
    with open("population-estimates.csv", "r") as csvfile:
        reader_variable = csv.DictReader(csvfile, delimiter=",")
        for population_data in reader_variable:
            country = population_data['Region']
            if country in countries:
                year = population_data['Year']
                if year == "2014":
                    population = population_data['Population']
                    asean_population[country] = population
    return asean_population


def display_population(country, population):
    plt.bar(country, population)
    plt.xlabel("country name")
    plt.ylabel("Population of asean countres in the year 2014")
    plt.xticks(rotation=30, ha='right')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    population_asean = read_data()
    display_population(population_asean.keys(), population_asean.values())
