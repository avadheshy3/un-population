import csv
import matplotlib.pylab as plt


def read_data():
    year = []
    population_india = []
    with open("population-estimates.csv", "r") as csvfile:
        reader_variable = csv.DictReader(csvfile, delimiter=",")
        for population in reader_variable:
            if(population['Region'] == 'India'):
                year.append(population['Year'])
                value = round(float(population['Population']), 2)
                population_india.append(value)
    return [year, population_india]


def display_india_population(year, population):
    plt.legend()
    plt.barh(year, population)
    plt.grid()
    plt.xlabel("India's population")
    plt.ylabel("year")
    plt.show()


if __name__ == '__main__':
    population_data = read_data()
    display_india_population(population_data[0], population_data[1])
