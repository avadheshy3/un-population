import csv
import matplotlib.pylab as plt
saarc_countries = ["Pakistan", "Maldives", "Sri Lanka", "Bhutan", "India",
                   "Afghanistan", "Bangladesh", "Nepal"]


def read_data():
    # fetching data from csv file
    saarc = {}
    with open("population-estimates.csv", "r") as csvfile:
        reader_variable = csv.DictReader(csvfile, delimiter=",")
        for pop_saarc in reader_variable:
            country = pop_saarc['Region']
            if country in saarc_countries:
                population = float(pop_saarc['Population'])
                year = int(pop_saarc['Year'])
                if country in saarc.keys():
                    saarc[country][year] = population
                else:
                    saarc[country] = {year: population}
    population_sum = [0 for year in range(1950, 2016)]
    for country in saarc_countries:
        for year in sorted(saarc[country].keys()):
            population_sum[year-1950] += saarc[country][year]
    return population_sum


def displayPopulation(populationSum):
    years = [i for i in range(1950, 2016)]
    plt.barh(years, populationSum)
    plt.xlabel("population of saarc countries")
    plt.ylabel("year")
    plt.show()


if __name__ == '__main__':
    population_sum = read_data()
    displayPopulation(population_sum)
