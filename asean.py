import csv
import matplotlib.pylab as plt
countries = ["Brunei Darussalam", "Cambodia", "Indonesia",
             "Malaysia", "Myanmar", "Philippines", "Singapore", "Thailand",
             "Viet Nam", "Lao People's Democratic Republic"]


def read_data():
    asean_countries = {}
    with open("population-estimates.csv", "r") as csvfile:
        reader_variable = csv.DictReader(csvfile, delimiter=",")
        for population in reader_variable:
            year = int(population['Year'])
            if 2004 <= year <= 2014:
                country = population['Region']
                if country in countries:
                    p_asean = float(population['Population'])
                    if year in asean_countries.keys():
                        asean_countries[year][country] = p_asean
                    else:
                        asean_countries[year] = {country: p_asean}
    return asean_countries


def display_asean_population(population):
    x = range(10)
    y = -0.2
    width = 0.04
    years = []
    for year in sorted(population.keys()):
        years.append(year)
        z = list(map(lambda a: a + y, x))
        plt.bar(z, population[year].values(), width)
        y += 0.05
    plt.legend(years)
    plt.xticks(x, countries, rotation=30)
    plt.xlabel("Years")
    plt.ylabel("Population")
    plt.show()


if __name__ == '__main__':
    population = read_data()
    display_asean_population(population)
